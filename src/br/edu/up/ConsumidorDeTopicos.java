package br.edu.up;

import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;
import javax.naming.Context;
import javax.naming.InitialContext;

public class ConsumidorDeTopicos {
  
  
  public static void main(String[] args) throws Exception {
    
    
    Context ctx = new InitialContext();
    Topic topico = (Topic) ctx.lookup("jms/TOPICO_DE_TESTE");
    ConnectionFactory cf = (ConnectionFactory) ctx.lookup("jms/RemoteConnectionFactory");
    
    Connection con = cf.createConnection("up", "positivo");
    //� preciso iniciar a conex�o no consumidor;
    con.start();
    Session session = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
    MessageConsumer consumidor = session.createConsumer(topico);
    
    System.out.println("Aguardando mensagem...");
    TextMessage msg = (TextMessage) consumidor.receive();
    System.out.println(msg.getText().toString()); 
    
    //� preciso encerrar a conex�o;
    con.close();
    ctx.close();
    
  }
}